@extends('layout.master')


@section('judul')
Media Online
@endsection



@section('content')
<h3>Sosial Media Developer</h3>
<p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
<h4>Benefit Join di Media Online</h4>
<ul type="disc">
    <li>Mendapatkan motivasi dari sesama Developer</li>
    <li>Sharing knowledge</li>
    <li>Dibuat oleh calon web developer terbaik</li>
</ul>
<h4>Cara Bergabung ke Media Online</h4>
<ol type ="1">
    <li>Mengunjungi Website ini</li>
    <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
    <li>Selesai</li>
</ol> 
@endsection

