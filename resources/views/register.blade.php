@extends('layout.master')


@section('judul')
Buat Account Baru!
@endsection



@section('content')
        
        <h3>Sign Up Form</h3>
        <form action="/welcome" method="post">
            @csrf
            <label >First Name: </label>
                <br><br>
            <input type="text" name="firstname">
                <br><br>
            <label for="lastname">Last Name:</label>
                <br><br>
            <input type="text" name="lastname">
                <br><br>
            <label >Gender:</label>
                <br><br>                
                <input type="radio"  name="gender" value="male"><label>Male</label>
                <br>
                <input type="radio"  name="gender" value="female"><label>Female</label>
                <br>
                <input type="radio"  name="gender" value="other"><label>Other</label>
                <br><br>
            <label>Nationality:</label>
                <br><br>
            <select name="negara">
                <option value="1">Indonesia</option>
                <option value="2">Amerika</option>
                <option value="3">Inggris</option>
            </select>
                <br><br>
            <label>Language Spoken:</label>
                <br><br>
            <input type="checkbox" name="languange">Bahasa Indonesia<br>
            <input type="checkbox" name="languange">English<br>
            <input type="checkbox" name="languange">Other
                <br><br>
            <label >Bio:</label>
                <br><br>
            <textarea name="Biodata"  cols="30" rows="10"></textarea>
                <br>
            <input type="submit" value="SignUp">        
        </form>
@endsection